# Welcome to your the SandBox documentation

This is a small sample book to give the background of the SandBox implementation.
It shows off a few of the major file types, as well as some sample content.


List of symbols
===============

| symbol       | indices    | definition                                     |
| -------      | ---        |------------------------------------------------|
| $p$          | -          | penalisation term (e.g. $ 10^{12}$)            |
| $V$          | -          | Area (or volume) of the REV in current state   |
| $V_0$        | -          | Area (or volume) of the REV in reference state |
|$\vec{x}$| $x_i$ | coordinates in current configuration |
|$\vec{y}$| $y_i$ | periodic vector in current configuration |
|$\vec{X}$| $X_i$ | coordinates in reference configuration |
|$\vec{Y}$| $Y_i$ | periodic vector in reference configuration |
| $\mathbf{F}$ | $F_{ij}$   | Deformation gradient tensor $\frac{\delta x_i}{\delta X_j}$ |                   |
| $\mathbf{\sigma}$ | $\sigma_{ij}$ |Cauchy stress tensor  |
| $^4\mathbf{C}$ | $C_{ijkl}$ | (consistent tangent) operator $C_{ijkl}\delta F_{kl}=\delta \sigma_{ij}$|


REV definition
==============
The representative elementary volume (REV) is defined is defined as a locally periodic representative sample of the mayterial microstructure.  

```{figure} ./images/REV.png
---
height: 300px
name: REV-figure
---
Deformed REV with ghost nodes $\vec{x}_g^{(i)}$ in reference configuration.
```



Finite element formulation
==========================
The finite element formulation in LAGAMINE is as follows:



* From the constitutive model, using true stress (Cauchy) and true strain (...) increments:

$$
^4\mathbf{C} : \delta \mathbf{e} = \delta \mathbf{\sigma}
$$

with 

$$
\delta \mathbf{e} = \left[\frac{\partial \delta u}{\partial x}\right] = \mathbf{F}^{-T}\cdot\delta F
$$ 

and (for example)

$$
^4\mathbf{C} = 
\frac{E}{(1+\nu)(1-2\nu)}
\begin{bmatrix}
1-\nu & 0 & 0 & \nu \\
0 & \frac{1-2\nu}{2} & \frac{1-2\nu}{2} & 0\\
0 & \frac{1-2\nu}{2} & \frac{1-2\nu}{2} & 0\\
\nu & 0 & 0 & 1-\nu \\
\end{bmatrix}
$$

* Tensors storred as vectors are saved row-major:

$$
\textbf{F} = 
\begin{bmatrix}
 \frac{\partial x_1}{\partial X_1} &  \frac{\partial x_1}{\partial X_2} \\
 \frac{\partial x_2}{\partial X_1} &  \frac{\partial x_2}{\partial X_2} \\
\end{bmatrix}
\rightarrow
\begin{bmatrix}
 \frac{\partial x_1}{\partial X_1} \\  \frac{\partial x_1}{\partial X_2} \\
 \frac{\partial x_2}{\partial X_1} \\  \frac{\partial x_2}{\partial X_2} \\
\end{bmatrix}
=
\begin{bmatrix}
 F_{11} \\  F_{12} \\ F_{21} \\  F_{22} \\
\end{bmatrix}

$$


* Correction of the compliance matrix by the stresses (corresponding to the )

$$
^4\mathbf{C}^*=
\begin{bmatrix}
    C_{1111} ~~~~~~~~~~    & C_{1112} - \sigma_{12} & C_{1121} ~~~~~~~~~~    & C_{1122} + \sigma_{11} \\
    C_{1211} + \sigma_{12} & C_{1212} ~~~~~~~~~~    & C_{1221} - \sigma_{11} & C_{1222} ~~~~~~~~~~    \\
    C_{2111} ~~~~~~~~~~    & C_{2112} - \sigma_{22} & C_{2121} ~~~~~~~~~~    & C_{2122} + \sigma_{12} \\
    C_{2211} + \sigma_{22} & C_{2212} ~~~~~~~~~~    & C_{2221} - \sigma_{12} & C_{2222} ~~~~~~~~~~ 
\end{bmatrix}
$$

* FE integration into element equations

$$
K^{(i,j)}_{jk} = \sum_{ip} w_{ip}\frac{\partial N_{ip}^{(i)}}{\partial x_i} C^*_{ijkl} \frac{\partial N_{ip}^{(j)}}{\partial x_l}|J_{ip}| 
$$

The Jacobian is defined as

$$
[J] = \left[\frac{\partial x_i}{\partial \xi_j}\right]
$$


* Assembly leads to the global system of linear equations to solve for:




$$
[\mathbf{K}_{glob}]\{\delta \mathbf{u}\} = \{\delta \mathbf{f}\}
$$





Periodic constraints
====================
Periodic constraints are applied through a penalisation of the relative dsplacement between periodically coupled REV boundary nodes $\vec{x}^+$ and $\vec{x}^-$ at a relative distance $\vec{y} = \vec{x}^+ - \vec{x}^-$. Vector $\vec{y}$ is the periodic vector, characterising the periodic dimensions of the REV. Penalisation of the relative displacement is formulated as:

$$
p (u_i^+ - u_i^-) - p~u_{\text{g},i}^{(j)}~Y_j = 0 
$$

where $p$ is a penalisation term and ghost nodes $(1)$ and $(2)$ are introduced for formulation convenience, with 

$$
\vec{X}_\text{g}^{(1)}=\begin{Bmatrix} 1 \\ 0\end{Bmatrix},\hspace{1cm}
\vec{X}_\text{g} ^{(2)}=\begin{Bmatrix} 0 \\ 1\end{Bmatrix}
$$

The displacements of the ghost nodes are prescribed explicitly, following:

$$
\vec{u}_\text{g}^{(i)} = (\mathbf{F} - \mathbf{I}) \vec{X}_\text{g}^{(i)}  
$$

in other words:

$$
x_{\text{g},i}^{(j)} = F_{ij}
$$

Through the penalisation of relative displacement against the two ghost nodes, no additional forces are introduced on the REV boundary nodes. 


Computational homogenisation
====


From the definition of stress, we have the following:


$$
\begin{equation}
\sigma^M_{ij} = \frac{1}{V} \sum_{(i)=3,4} f^{(i)}_i x^{(i)}_j \tag{1}
\end{equation}
$$

The macroscopic displacement gradient $\mathbf{F}$ tensor is defined as:

$$
F_{ij} = \frac{\partial x_i}{\partial X_j} \tag{2}
$$

This means that the coordinated of the deformed configuration $\vec{x}$ can be evaluated basd on the coordinates of the reference configuration $\vec{X}$:

$$
x_j = F_{jk} X_{k} \tag{3} 
$$

The volume of the $1\times1$ REV is evaluated based on the macroscopic displacement gradient tensor:

$$
V = |F| = F_{11}F_{22}-F_{12}F_{21} \tag{4}
$$

The variational term follows from there:

$$
\delta V = V F^{-T} :\delta F = V (F^{-1})_{ji}\delta F_{ij} = 
$$


$$
\delta\left(\frac{1}{V}\right) = -\frac{1}{V} \text{tr}(\mathbf{\delta e})
$$


$$
\delta \sigma^M_{ij} = \frac{1}{V}\sum_{(i)} \delta f_i^{(i)} x^{(i)}_j + f_i^{(i)}\delta x_j^{(i)}  = \sum_{(i)} \delta f_i^{(i)} e_{jk} x^{(i)}_k + f_i^{(i)}\delta e_{jk} x_k^{(i)} \\
$$


$$
\begin{equation}
\delta u_i = \delta F_{ij} Y_j = \delta \varepsilon^M_{ij} y_j
\end{equation}
$$

From the finite element system of equations for an equilibrated system, we have

$$
\begin{bmatrix}
    \mathbf{K}_{ff} & \mathbf{K}_{fp} \\
    \mathbf{K}_{pf} & \mathbf{K}_{pp}
\end{bmatrix}
\begin{Bmatrix}
    \delta \mathbf{u}_f \\
    \delta \mathbf{u}_p \\
\end{Bmatrix}
=
\begin{Bmatrix}
    \mathbf{0} \\
    \delta \mathbf{f}_p \\
\end{Bmatrix}
$$

with subscripts $_f$ and $_p$ indicating the free and prescribed degrees of freedom respectively. Note that,in oderr to solve for the equilibrium of the REV, the linearised system $[\textbf{K}_{ff}] \{\Delta \mathbf{u}_f \} = \{\mathbf{f}_{res} \}$ has been solved, and (when a direct solver is used) the factorisation of $[\mathbf{K}_{ff}]$ has been evaluated.

Static condensation on the prescribed degrees of freedom leads to:

$$
\delta \mathbf{f}_p = \left(\mathbf{K}_{pp} - \mathbf{K}_{pf}\mathbf{K}_{ff}^{-1}\mathbf{K}_{fp}\right) \delta \mathbf{u}_p = \textbf{S} \delta \mathbf{u}_p  
$$

With $\mathbf{f}_p$ containing only the reaction forces on the ghost nodes (which are the only prescribed displacements) we have:

$$
\delta \mathbf{u}_p = 
\begin{Bmatrix}
    \delta u_1^{(1)} \\ \delta u_2^{(1)} \\ \delta u_1^{(2)} \\ \delta u_2^{(2)} \\
\end{Bmatrix}, 
\hspace{1cm}
\delta \mathbf{f}_p = 
\begin{Bmatrix}
    \delta f_1^{(1)} \\ \delta f_2^{(1)} \\ \delta f_1^{(2)} \\ \delta f_2^{(2)} \\
\end{Bmatrix}
$$


---

We start from the homogenised stress tensor for an REV in rotational equilibrium: 

$$
\sigma^M_{ij} = \frac{1}{V}\sum_{(i)}f_i^{(i)}x_j^{(i)}
$$

the variational form gives:

$$
\delta \sigma^M_{ij} = -\frac{1}{V^2}\delta V \sum_{(i)}f_i^{(i)}x_j^{(i)} 
+ \frac{1}{V}\sum_{(i)}\delta f_i^{(i)}x_j^{(i)} 
+ \frac{1}{V}\sum_{(i)}f_i^{(i)} \delta x_j^{(i)} 
$$


Expressing all variations in terms of $\delta \mathbf{\varepsilon}$ requires the following:

$$
\delta V = V \text{tr}(\delta \varepsilon)=V \mathbf{I}:\delta\varepsilon,~~~~~~~~\delta \vec{f}^{(i)} = \mathbf{A}^{(i,j)}\cdot\delta \vec{u}^{(j)}=\sum_{(j)}\mathbf{A}^{(i,j)}\cdot\delta\varepsilon \cdot\vec{x}^{(j)},~~~~~~~~\delta \vec{x}^{(i)}=\delta \varepsilon\cdot \vec{x}^{(i)}
$$

Substitution of these equations results in the constitutive component of the stiffness matrix $\delta \mathbf{\sigma}=^4\mathbf{C}^M:\delta\varepsilon$:

$$
^4\mathbf{C}^M = \frac{1}{V}\sum_{(i)}\sum_{(j)}y_j^{(i)} A_{ik}^{(i,j)}y_l^{(j)} + \frac{1}{V}\sum_{(i)}f_i^{(i)}I_{jk}y_{l}^{(i)}  - \sigma^M_{ij}I_{kl} 
$$
$$
^4\mathbf{C}^M = \frac{1}{V}\sum_{(i)}\sum_{(j)}y_j^{(i)} A_{ik}^{(i,j)}y_l^{(j)} + \sigma_{il}^M I_{jk}  - \sigma^M_{ij}I_{kl} \\
$$

Note that the second and third components combined are equivalent to the stress terms that are added to the compliance matrix of the constitutive law in the element routine.




THE END
===

Check out the content pages bundled with this sample book to see more.

```{tableofcontents}
```

